#!/usr/bin/env python
# -*- coding: utf-8 -*-

import shutil
import numpy as np
from scipy.optimize import differential_evolution
import h5py

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from spi_correct_amplitude import report_lib



def read_amplitude(cxi_file):
    """Read 3D amplitude from CXI file"""
    with h5py.File(cxi_file, 'r') as h5file:
        data = h5file['entry_1/image_1/data'][:]

    if np.isnan(data).any():
        raise ValueError('Data contain NaN.')
    return data


def save_amplitude(data, old_file, new_file):
    """Save copy of CXI file with updated 3D amplitude"""
    shutil.copyfile(old_file, new_file)
    with h5py.File(new_file, 'a') as h5file:
        dset = h5file['entry_1/image_1/data']
        dset[:] = data


def compute_r_grid(shape):
    """Compute array with values = radius from center"""
    assert len(shape) == 3

    size_z, size_y, size_x = shape

    center_x = (size_x - 1) // 2
    center_y = (size_y - 1) // 2
    center_z = (size_z - 1) // 2
    x_array = np.arange(size_x) - center_x
    y_array = np.arange(size_y) - center_y
    z_array = np.arange(size_z) - center_z

    x_grid, y_grid, z_grid = np.meshgrid(x_array, y_array, z_array)
    return np.sqrt(x_grid**2 + y_grid**2 + z_grid**2)


def compute_radius_min(data):
    """Compute minimum data value for each radius"""
    r_grid = compute_r_grid(data.shape).astype(int)

    size_z, size_y, size_x = data.shape
    center_x = (size_x - 1) // 2
    center_y = (size_y - 1) // 2
    center_z = (size_z - 1) // 2
    r_max = max(center_x, center_y, center_z)

    result = np.ones(r_max) * np.amax(data)

    for i in range(r_max):
        result[i] = data[r_grid == i].min()

    r_max_final = r_max - np.argmax(result[::-1] > 0)

    return result[:r_max_final+1]


def gauss_plus_const(x, amplitude, sigma, const):
    """Return gauss values at x coordinates with given parameters"""
    gauss = np.exp(-(x**2 / ( 2.0 * sigma**2 )))
    return amplitude * gauss  + const


def fit_gauss_plus_const(data):
    """Find best fitting parameters for gauss plus const"""
    assert data.ndim == 1
    max_r = len(data)

    xdata = np.arange(max_r//2, max_r)
    fit_data = data[xdata]

    amp_lim = (0, data.max())
    sigma_lim = (max_r // 10, max_r)
    const_lim = (0, data[max_r//2])

    def fit_error(x):
        diff = fit_data - gauss_plus_const(xdata, x[0], x[1], x[2])
        # diff[diff < 0] = fit_data.max() # Punish overestimation of background
        return np.sum(diff**2)

    bounds = [amp_lim, sigma_lim, const_lim]
    result = differential_evolution(fit_error, bounds)
    return result.x


def compute_background_values(shape, amplitude, sigma, const):
    """Return 3D gauss background"""
    r_grid = compute_r_grid(shape)
    return gauss_plus_const(r_grid, amplitude, sigma, const)


def plot_3_slices(data, ax, logscale=True, vmin=None):
    report_lib.plot_image(data[data.shape[0]//2], ax[0], logscale=logscale, vmin=vmin)
    report_lib.plot_image(data[:, data.shape[1]//2], ax[1], logscale=logscale, vmin=vmin)
    report_lib.plot_image(data[:, :, data.shape[2]//2], ax[2], logscale=logscale, vmin=vmin)


def generate_report(report_name, old_data, new_data, radius_min, amplitude, sigma, const):
    report = report_lib.ReportWriter(report_name)

    plot_min = const/2
    if plot_min <= 0:
        logscale = False
    else:
        logscale = True

    fig, ax = plt.subplots(1, 3, figsize=(12, 4))
    plot_3_slices(old_data, ax, logscale, plot_min)
    ax[1].set_title('Input amplitudes')
    fig.tight_layout()
    report.save_figure(fig)

    fig, ax = plt.subplots(1, 3, figsize=(12, 4))
    plot_3_slices(new_data, ax, logscale, plot_min)
    ax[1].set_title('Corrected amplitudes')
    fig.tight_layout()
    report.save_figure(fig)

    fig, ax = plt.subplots(figsize=(12, 4))
    ax.plot(radius_min, label='Minimal intensity per radius')
    ax.plot(gauss_plus_const(np.arange(len(radius_min)), amplitude, sigma, const),
            ls='--', color='r', label='Estimated background')
    ax.set_yscale('log')
    ax.legend()
    ax.set_ylabel('Intensity [a.u.]')
    ax.set_xlabel('Radius [px]')
    fig.tight_layout()
    report.save_figure(fig)

    report.close()
