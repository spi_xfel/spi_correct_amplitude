# Installation

Processing scripts require **Python 3.6** or higher.

You can install package by:
```
cd spi_correct_amplitude
pip install .
```

# File format

All scripts work with data in [CXI format](https://www.cxidb.org/cxi.html).
The following internal structure is expected:
```
file.cxi 
   ├─ cxi_version
   └─ entry_1
       └─ image_1
           └─ data (Nx, Ny, Nz)

```
Dataset **data** should contain 3D diffraction image extracted from Dragonfly output.

# Correct EMC result

Correct 3D amplitudes in EMC result by subtraction of estimated background.

```
spi_correct.py [-h] -o OUT -m {const,gauss} FILE
```

Options:
* `FILE` - Input CXI file with 3D amplitudes (intensities)
* `-o OUT` - output CXI file
* `-m {const,gauss}` - shape of subtracted background: const or const + gauss.
