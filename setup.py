from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='spi_correct_amplitude',
      version='0.1',
      description='Module to correct 3D amplitude in CXI file by gauss+const background estimation',
      long_description=readme(),
      long_description_content_type="text/markdown",
      url='https://gitlab.com/spi_xfel/spi_correct_amplitude',
      author='Sergey Bobkov',
      author_email='s.bobkov@grid.kiae.ru',
      license='MIT',
      python_requires='>=3.6',
      install_requires=['numpy',
                        'scipy',
                        'h5py',
                        'matplotlib'],
      packages=['spi_correct_amplitude'],
      scripts=['scripts/spi_correct.py'],
      zip_safe=False)
