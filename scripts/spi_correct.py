#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse

from spi_correct_amplitude import correct

PDF_REPORT_NAME = 'correct_amplitude_report.pdf'


def main():
    parser = argparse.ArgumentParser(
        description='Correct background in 3D amplitudes for phase retrieval.')
    parser.add_argument('cxi_file', metavar='FILE', help='Input cxi file with 3D amplitudes')
    parser.add_argument('-o', '--out', required=True, help="Output file")
    parser.add_argument('-m', '--mode', required=True, choices=['const', 'gauss'],
                        help="Correction mode")
    parser.add_argument('-l', '--limit', type=int,
                        help='Maximum allowed value, bigger values will be masked')
    parser.add_argument('-r', '--report', default=PDF_REPORT_NAME, help="Report file")
    parser_args = parser.parse_args()

    cxi_file = parser_args.cxi_file
    out_file = parser_args.out
    corr_mode = parser_args.mode
    limit = parser_args.limit
    report_file = parser_args.report

    if not os.path.isfile(cxi_file):
        parser.error("File {} doesn't exist".format(cxi_file))

    if os.path.exists(out_file) and not os.path.isfile(out_file):
        parser.error("{} is a directory".format(out_file))

    amp_data = correct.read_amplitude(cxi_file)
    radius_min = correct.compute_radius_min(amp_data)
    amplitude, sigma, const = correct.fit_gauss_plus_const(radius_min)

    if corr_mode != 'gauss':
        amplitude = 0

    background = correct.compute_background_values(amp_data.shape, amplitude, sigma, const)

    new_data = amp_data - background
    new_data[new_data < 0] = 0

    if limit:
        new_data[amp_data > limit] = 0

    correct.save_amplitude(new_data, cxi_file, out_file)

    correct.generate_report(report_file, amp_data, new_data, radius_min, amplitude, sigma, const)


if __name__ == '__main__':
    main()
